   ___       _       _                  ___                             _          
  /___\_ __ (_)_ __ (_) ___  _ __      /   \_   _ _ __   __ _ _ __ ___ (_) ___ ___ 
 //  // '_ \| | '_ \| |/ _ \| '_ \    / /\ / | | | '_ \ / _` | '_ ` _ \| |/ __/ __|
/ \_//| |_) | | | | | | (_) | | | |  / /_//| |_| | | | | (_| | | | | | | | (__\__ \
\___/ | .__/|_|_| |_|_|\___/|_| |_| /___,'  \__, |_| |_|\__,_|_| |_| |_|_|\___|___/
      |_|                                   |___/                                  

      by Zachary Nado

ChartPanel.java - renders the bar graph for the opinion levels
Constants.java - various model constants
Diffusions.java - assorted diffusion functions that return a constant for all opinion levels
VaryingDiffusions.java - assorted diffusion fucntions that return a diffusion constant for individual opinion levels for the same timestep
Util.java - various helper functions, including party detectin
Simulator.java - the numerical solver system
Party.java - a simple object that defines a political party
Main.java - used to run the JApplet
