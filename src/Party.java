import java.util.ArrayList;

public class Party {

  public int id;
  public ArrayList<Integer> indexes;
  public ArrayList<Integer> peakHistory;
  private int peak;
  public int startIteration = 0;
  public int endIteration = -1;

  // only used in party detection
  public boolean modified = false;

  public Party(ArrayList<Integer> indexes, int peak, int startIteration) {
    this.id = (indexes.hashCode() + Long.toString(System.currentTimeMillis())).hashCode();
    this.indexes = indexes;
    this.peak = peak;
    this.startIteration = startIteration;
    peakHistory = new ArrayList<Integer>();
  }

  public int getPeak() {
    return peak;
  }

  public void setPeak(int peak) {
    // don't add the same peak more than once in a row
    if(peakHistory.isEmpty() || peakHistory.get(peakHistory.size() - 1) != peak) {
      this.peakHistory.add(peak);
    }
    this.peak = peak;
  }

  public void end(int endIteration) {
    this.endIteration = endIteration;
  }

  public void finish(int endIteration) {
    if(this.endIteration == -1) {
      this.endIteration = endIteration;
    }
  }

  public int lifespan() {
    return endIteration - startIteration;
  }

  public boolean isEnded() {
    return this.endIteration != -1;
  }

  @Override
  public String toString() {
    return "Party{" +
      "peakHistory=" + peakHistory +
      ", currentPeak=" + peak +
      ", startIteration=" + startIteration +
      ", endIteration=" + endIteration +
      '}';
  }
}
