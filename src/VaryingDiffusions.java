import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VaryingDiffusions {


  // also really cool
  // with cooldown cap high, cuz have more init pop on right, it will soon make a right extremist party that will overtake
  // the more right initial parties
  // with cooldown cap low, will make this new extreme right party, but depending on when stop it the right most initial
  // parties can take back control and consume it as well
  //
  // should calculate what cutoff makes the behavior change (bifurcate?)
  public static boolean cooldownL = false, cooldownR = false;
  public static VaryingDiffusionFunction monitoredExtremismWithCap = new VaryingDiffusionFunction() {
    @Override
    public ArrayList<Double> diffuse(List<Double> opinions) {

      // allow diffusion as normal with a constant d but when get enough in the tails of spectrum choose
      // middle of "tail" to start rising
      double d = 17.0;
      double totalPopulation = 0.0;
      for(int i=0; i<opinions.size(); i++) {
        totalPopulation += opinions.get(i);
      }
      int fifth = opinions.size()/5, tenth = fifth/2;

      double left = 0.0, right = 0.0, leftD = d, rightD = d;
      for(int i=0; i<fifth; i++) {
        left += opinions.get(i);
      }
      for(int i=opinions.size() - fifth; i<opinions.size(); i++) {
        right += opinions.get(i);
      }

      // need to cool down
      if(left >= totalPopulation/3) {
        haveMadeLeftParty = false;
        cooldownL = true;
      }
      // need to cool down
      if(right >= totalPopulation/3) {
        haveMadeRightParty = false;
        cooldownR = true;
      }

      // can pick it back up again
      if(left <= totalPopulation/50) {
        haveMadeLeftParty = false;
        cooldownL = false;
      }
      // can pick it back up again
      if(right <= totalPopulation/50) {
        haveMadeRightParty = false;
        cooldownR = false;
      }

      if(left >= totalPopulation/50 && !haveMadeLeftParty && !cooldownL) {
        spaceSize = opinions.size();
        haveMadeLeftParty = true;
        leftIndex = tenth;
      }
      if(right >= totalPopulation/50 && !haveMadeRightParty && !cooldownR) {
        spaceSize = opinions.size();
        haveMadeRightParty = true;
        rightIndex = opinions.size() - tenth;
      }

      ArrayList<Double> diffs = Util.listBuilder(Arrays.asList(new Util.Tuple(opinions.size(), d)));
      if(haveMadeLeftParty) {
        if(opinions.size() != spaceSize) {
          leftIndex += (opinions.size() - spaceSize)/2;
        }
        double nm2 = opinions.get(leftIndex-2);
        double nm1 = opinions.get(leftIndex-1);
        double n = opinions.get(leftIndex);
        double n1 = opinions.get(leftIndex+1);
        double n2 = opinions.get(leftIndex+2);
        leftD = makeConstantChange(2, nm2, nm1, n, n1, n2);
        diffs.set(leftIndex, leftD);
      }
      if(haveMadeRightParty) {
        if(opinions.size() != spaceSize) {
          rightIndex += (opinions.size() - spaceSize)/2;
        }
        double nm2 = opinions.get(rightIndex-2);
        double nm1 = opinions.get(rightIndex-1);
        double n = opinions.get(rightIndex);
        double n1 = opinions.get(rightIndex+1);
        double n2 = opinions.get(rightIndex+2);
        rightD = makeConstantChange(2, nm2, nm1, n, n1, n2);
        diffs.set(rightIndex, rightD);
      }
      spaceSize = opinions.size();
      return diffs;
    }
  };



  // reverses fate of 4 party system! rightmost tiny party ends up swallowing larger less extreme one!
  // remnants of the larger, less extreme party can be seen later on in the systems development
  // still gets carried away and the deriv of the spawned party cores blows up cuz don't check them
  private static boolean haveMadeLeftParty = false;
  private static boolean haveMadeRightParty = false;
  private static int spaceSize = 0;
  private static int leftIndex = 0;
  private static int rightIndex = 0;

  public static VaryingDiffusionFunction monitoredExtremism = new VaryingDiffusionFunction() {
    @Override
    public ArrayList<Double> diffuse(List<Double> opinions) {
      // allow diffusion as normal with a constant d but when get enough in the tails of spectrum choose
      // middle of "tail" to start rising
      double d = 17.0;
      double totalPopulation = 0.0;
      for(int i=0; i<opinions.size(); i++) {
        totalPopulation += opinions.get(i);
      }
      int fifth = opinions.size()/5, tenth = fifth/2;

      double left = 0.0, right = 0.0, leftD = d, rightD = d;
      for(int i=0; i<fifth; i++) {
        left += opinions.get(i);
      }
      for(int i=opinions.size() - fifth; i<opinions.size(); i++) {
        right += opinions.get(i);
      }
      if(left >= totalPopulation/50 && !haveMadeLeftParty) {
        spaceSize = opinions.size();
        haveMadeLeftParty = true;
        leftIndex = tenth;
      }
      if(right >= totalPopulation/50 && !haveMadeRightParty) {
        spaceSize = opinions.size();
        haveMadeRightParty = true;
        rightIndex = opinions.size() - tenth;
      }
      ArrayList<Double> diffs = Util.listBuilder(Arrays.asList(new Util.Tuple(opinions.size(), d)));
      if(haveMadeLeftParty) {
        if(opinions.size() != spaceSize) {
          leftIndex += (opinions.size() - spaceSize)/2;
        }
        double nm2 = opinions.get(leftIndex-2);
        double nm1 = opinions.get(leftIndex-1);
        double n = opinions.get(leftIndex);
        double n1 = opinions.get(leftIndex+1);
        double n2 = opinions.get(leftIndex+2);
        leftD = makeConstantChange(2, nm2, nm1, n, n1, n2);
        diffs.set(leftIndex, leftD);
      }
      if(haveMadeRightParty) {
        if(opinions.size() != spaceSize) {
          rightIndex += (opinions.size() - spaceSize)/2;
        }
        double nm2 = opinions.get(rightIndex-2);
        double nm1 = opinions.get(rightIndex-1);
        double n = opinions.get(rightIndex);
        double n1 = opinions.get(rightIndex+1);
        double n2 = opinions.get(rightIndex+2);
        rightD = makeConstantChange(2, nm2, nm1, n, n1, n2);
        diffs.set(rightIndex, rightD);
      }
      spaceSize = opinions.size();
      return diffs;
    }
  };

  // interesting, keeps making extreme parties though which end up competing for the lead
  // in the extremes
  // also takes much longer to blow up (3.5x longer than improved) cuz the extreme parties are
  // so spread out
  public static VaryingDiffusionFunction naiveExtremism = new VaryingDiffusionFunction() {
    @Override
    public ArrayList<Double> diffuse(List<Double> opinions) {
      // allow diffusion as normal with a constant d but when get enough in the tails of spectrum choose
      // middle of "tail" to start rising
      double d = 7.0;
      double totalPopulation = 0.0;
      for(int i=0; i<opinions.size(); i++) {
        totalPopulation += opinions.get(i);
      }
      int fifth = opinions.size()/5, tenth = fifth/2;
      double left = 0.0, right = 0.0, leftD = d, rightD = d;
      for(int i=0; i<fifth; i++) {
        left += opinions.get(i);
      }
      for(int i=opinions.size() - fifth; i<opinions.size(); i++) {
        right += opinions.get(i);
      }
      if(left >= totalPopulation/50) {
        double nm2 = opinions.get(tenth-2);
        double nm1 = opinions.get(tenth-1);
        double n = opinions.get(tenth);
        double n1 = opinions.get(tenth+1);
        double n2 = opinions.get(tenth+2);
        leftD = makeConstantChange(2, nm2, nm1, n, n1, n2);
      }
      if(right >= totalPopulation/50) {
        double nm2 = opinions.get(opinions.size() - tenth-2);
        double nm1 = opinions.get(opinions.size() - tenth-1);
        double n = opinions.get(opinions.size() - tenth);
        double n1 = opinions.get(opinions.size() - tenth+1);
        double n2 = opinions.get(opinions.size() - tenth+2);
        rightD = makeConstantChange(2, nm2, nm1, n, n1, n2);
      }
      ArrayList<Double> diffs = Util.listBuilder(Arrays.asList(new Util.Tuple(opinions.size(), d)));
      diffs.set(tenth, leftD);
      diffs.set(opinions.size() - tenth, rightD);
      return diffs;

    }
  };

  public static VaryingDiffusionFunction noChange = new VaryingDiffusionFunction() {
    @Override
    public ArrayList<Double> diffuse(List<Double> opinions) {
      ArrayList<Double> d_n = new ArrayList<Double>(opinions.size());
      d_n.add(0.0);
      d_n.add(0.0);
      for(int n=2; n<opinions.size() - 2; n++) {
        double nm2 = opinions.get(n-2);
        double nm1 = opinions.get(n-1);
        double n0 = opinions.get(n);
        double n1 = opinions.get(n+1);
        double n2 = opinions.get(n+2);

        double denominator = nm1+n1-2*n0;
        if(denominator == 0.0) {
          d_n.add(0.0);
        } else {
          d_n.add((n0*(nm2+n2)-2*nm1*n1)/denominator);
        }
      }
      d_n.add(0.0);
      d_n.add(0.0);
      //System.out.println(Arrays.toString(d_n.toArray()));
      return d_n;
    }
  };

  // makes a triangle plus initial conditions, because when the extremes get nonzero
  // it expands, but now the extremes are c higher, and repeats, so makes pyramid
  // shape with the initial conditions on top of it
  public static VaryingDiffusionFunction constantChange = new VaryingDiffusionFunction() {
    @Override
    public ArrayList<Double> diffuse(List<Double> opinions) {
      double c=10;
      ArrayList<Double> d_n = new ArrayList<Double>(opinions.size());
      d_n.add(0.0);
      d_n.add(0.0);
      for(int n=2; n<opinions.size() - 2; n++) {
        double nm2 = opinions.get(n-2);
        double nm1 = opinions.get(n-1);
        double n0 = opinions.get(n);
        double n1 = opinions.get(n+1);
        double n2 = opinions.get(n+2);
        d_n.add(makeConstantChange(c, nm2, nm1, n0, n1, n2));
      }
      d_n.add(0.0);
      d_n.add(0.0);
      return d_n;
    }
  };

  public static double makeConstantChange(double change, double nm2, double nm1, double n, double n1, double n2) {
    double denominator = nm1+n1-2*n;
    if(denominator == 0) {
      return 0.0;
    } else {
      return (change + n*(nm2+n2)-2*nm1*n1)/denominator;
    }
  }
}