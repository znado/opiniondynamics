import java.util.List;

public class Diffusions {


  public static DiffusionFunction constant = new DiffusionFunction() {
    @Override
    public double diffuse(List<Double> opinions) {
      return 7.;
    }
  };

  static int offset = 2;
  static int passed = 0;

  public static DiffusionFunction noChange = new DiffusionFunction() {
    @Override
    public double diffuse(List<Double> opinions) {
      passed++;
      if(passed < 700) {
        return 17.0;
      }
      int index = offset;
      double nm2 = opinions.get(index-2);
      double nm1 = opinions.get(index-1);
      double n0 = opinions.get(index);
      double n1 = opinions.get(index+1);
      double n2 = opinions.get(index+2);
      offset++;
      offset = (offset > opinions.size() - 3) ? 2*opinions.size()/3 : offset;
      double denominator = nm1+n1-2*n0;
      if(denominator == 0.0) {
        return 7.0;
      } else {
        return (n0*(nm2+n2)-2*nm1*n1)/denominator;
      }
    }
  };

  // makes a rounder hill than the next two
  public static DiffusionFunction toExtremes = new DiffusionFunction() {
    @Override
    public double diffuse(List<Double> opinions) {
      int quarter = opinions.size()/4;
      double middleMass = 0.0;
      for(int i=quarter; i<3*quarter; i++) {
        middleMass += opinions.get(i);
      }
      return middleMass;
    }
  };

  // this and next one will eventually settle on a sharper mound in the middle, with intermediary
  // stated dependent on initial conditions
  public static DiffusionFunction conservativeMoney = new DiffusionFunction() {
    @Override
    public double diffuse(List<Double> opinions) {
      // use the mass of the first third as the diffusion
      int cutoffIndex = opinions.size()/3;
      double liberalMass = 0.0;
      for(int i=0; i<cutoffIndex; i++) {
        liberalMass += opinions.get(i);
      }
      return liberalMass;
    }
  };

  public static DiffusionFunction liberalYouth = new DiffusionFunction() {
    @Override
    public double diffuse(List<Double> opinions) {
      // use the mass of the last third as the diffusion
      int cutoffIndex = 2*opinions.size()/3;
      double liberalMass = 0.0;
      for(int i=cutoffIndex; i<opinions.size(); i++) {
        liberalMass += opinions.get(i);
      }
      return liberalMass;
    }
  };

  // makes no sense, goes to NaN for anything in magnitude larger than this
  public static DiffusionFunction negative = new DiffusionFunction() {
    @Override
    public double diffuse(List<Double> opinions) {
      return -0.05;
    }
  };
}
