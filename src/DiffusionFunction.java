import java.util.List;

public interface DiffusionFunction {
  public double diffuse(List<Double> opinions);
}
