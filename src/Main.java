import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

// TODO numerically analyze when to cooldown for extremism
// TODO create spawner that forms parties in between larger ones that represents compromise
// TODO jar to run?


public class Main extends JApplet {

  public void init() {
    Simulator simulator = new Simulator();
    // multi-party spread out
//    ArrayList<Double> pop = Util.listBuilder(new ArrayList<Util.Tuple>(Arrays.asList(new Util.Tuple(90, 0), new Util.Tuple(1, 2*Constants.m), new Util.Tuple(1, Constants.m), new Util.Tuple(1, 2*Constants.m), new Util.Tuple(9, 0), new Util.Tuple(1, Constants.m), new Util.Tuple(1, Constants.m), new Util.Tuple(90, 0),
//                                                                                 new Util.Tuple(1, 4*Constants.m), new Util.Tuple(1, 4*Constants.m), new Util.Tuple(9, 0), new Util.Tuple(1, 0.4*Constants.m), new Util.Tuple(1, 0.4*Constants.m), new Util.Tuple(90, 0))));

    // multi-party close
    ArrayList<Double> pop = Util.listBuilder(new ArrayList<Util.Tuple>(Arrays.asList(
      new Util.Tuple(20, 0),
      new Util.Tuple(1, 2*Constants.m),
      new Util.Tuple(1, Constants.m),
      new Util.Tuple(1, 2*Constants.m),
      new Util.Tuple(9, 0),
      new Util.Tuple(1, Constants.m),
      new Util.Tuple(1, Constants.m),
      new Util.Tuple(90, 0),
      new Util.Tuple(1, 4*Constants.m),
      new Util.Tuple(1, 4*Constants.m),
      new Util.Tuple(9, 0),
      new Util.Tuple(1, 0.4*Constants.m),
      new Util.Tuple(1, 0.4*Constants.m),
      new Util.Tuple(20, 0))));

    // multi-party close reversed
//    ArrayList<Double> pop = Util.listBuilder(new ArrayList<Util.Tuple>(Arrays.asList(
//      new Util.Tuple(20, 0),
//      new Util.Tuple(1, 4*Constants.m),
//      new Util.Tuple(1, 4*Constants.m),
//      new Util.Tuple(9, 0),
//      new Util.Tuple(1, 0.4*Constants.m),
//      new Util.Tuple(1, 0.4*Constants.m),
//      new Util.Tuple(90, 0),
//      new Util.Tuple(1, 2*Constants.m),
//      new Util.Tuple(1, Constants.m),
//      new Util.Tuple(1, 2*Constants.m),
//      new Util.Tuple(9, 0),
//      new Util.Tuple(1, Constants.m),
//      new Util.Tuple(1, Constants.m),
//      new Util.Tuple(20, 0))));

    // single party
//    ArrayList<Double> pop = Util.listBuilder(new ArrayList<Util.Tuple>(Arrays.asList(new Util.Tuple(9, 0), new Util.Tuple(1, 2 * Constants.m), new Util.Tuple(1, 2 * Constants.m),
//      new Util.Tuple(9, 0))));

    // two party
//    ArrayList<Double> pop = Util.listBuilder(new ArrayList<Util.Tuple>(Arrays.asList(new Util.Tuple(9, 0), new Util.Tuple(1, 2 * Constants.m), new Util.Tuple(1, 2 * Constants.m), new Util.Tuple(9, 0),
//      new Util.Tuple(28, 0), new Util.Tuple(1, Constants.m), new Util.Tuple(1, Constants.m), new Util.Tuple(9, 0))));

    simulator.run(pop, Constants.iters, VaryingDiffusions.constantChange);
  }
}

// issue with eulers step size
// issue with boundary conds messing up things next to them - loose population cuz it would be more extreme but now not considered, thus intelligently expand when above threshold next to extremes (include population loss rates)
// issue with not working - issue with going to even flat state - + when shouldve had *
// issue with rounding error in calculations

// normal distribution with parties n heat eq
// entropy?

// orig using party mass to detect parties but now use increase/decrease and trim tails cuz this more accurate
// orig was using party mass to remove parties but this taking too long cuz wide short parties could have mass 2 easily so used max cutoff instead (discuss high low high party edge case, how I believe is two parties)


// first make orig model
// then party detect
// then experiment with D
// then look for where parties pop up and stay the longest