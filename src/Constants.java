public class Constants {
  // the max number of iterations to run for
  public static final int iters = 2;

  // the number of people in an opinion column in a party
  public static final int m = 10;

  // if changing by this much then start a party
  public static final double partyChangeThreshold = 0.009;

  // remove opinion values that contribute less than this much to the party (where 1.0 is the whole party)
  public static final double partyMassThreshold = 0.01;

  // the number of ms to pause for for each iteration
  public static final int displayPause = 200;

  // the value the extremes have to be at in order to expand the opinion space
  public static final double expandThreshold = 0.000001;

  // a party must have an opinion with more people than this to be a party
  public static final double partyMaxThreshold = 1.0;

  // display the applet
  public static final boolean display = true;

  // same party peak difference threshold
  public static final int peakDiffThreshold = 4;
}