import java.util.ArrayList;
import java.util.List;

public interface VaryingDiffusionFunction {
  // returns a list of rates of changes for the opinion values at the index
  // in the opinion space that is the same as its index in the array
  public ArrayList<Double> diffuse(List<Double> opinions);
}